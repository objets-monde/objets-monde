#/bin/bash
#sleep 1

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

kill_soft()
{
	echo "kill vvvideo"
	killall vvvideo
	echo "kill pd"
	killall pd
	echo "kill Chataigne"
	killall Chataigne
	echo "kill REAPER"
	killall REAPER
}

start_vvvideo()
{
	cd $SCRIPTPATH/../../../../../vvvideo/of/vvvideo/bin/
	open vvvideo.app
	#open vvvideoDebug.app
}

start_chataigne()
{
	cd $SCRIPTPATH/../../../../../clickrotaread/chataigne/
	open objetsmonde-safran.noisette
}

start_pd()
{
	cd $SCRIPTPATH/../../../../../clickrotaread/pd
	open clickrotaread.pd
}

start_reaper()
{
	cd $SCRIPTPATH/../../../../../oxider/reaper/
	open objets-monde_safran.RPP
}

kill_soft
sleep 1
start_pd
sleep 1
start_reaper
sleep 1
start_chataigne
sleep 5
start_vvvideo

