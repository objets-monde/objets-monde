#!/bin/bash

min_vol=10
normal_vol=90

current_vol=$(osascript -e 'output volume of (get volume settings)')

if [ "$min_vol" -gt "$current_vol" ]; then

	echo "Volume is abnormaly low, raising it to $normal_vol"
	osascript -e "set volume output volume $normal_vol"
    
fi
