#/bin/bash
#sleep 1

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

kill_soft()
{
	echo "kill vvvideo"
	killall vvvideo
	echo "kill pd"
	killall pd
	echo "kill Chataigne"
	killall Chataigne
	echo "kill REAPER"
	killall REAPER
}

start_vvvideo()
{
    cd $SCRIPTPATH/../../../../../vvvideo/vvvideo/bin/
	open vvvideo.app
}

start_chataigne()
{
	cd $SCRIPTPATH/../../../../../om-chataigne/
	open objetsmonde-safran.noisette
}

start_pd()
{
	cd $SCRIPTPATH/../../../../../om-pd
	open click_rota_read_safran.pd
}

start_reaper()
{
	cd $SCRIPTPATH/../../../../../oxider/reaper/
	open objets-monde_safran.RPP
}

handle_volume()
{
	min_vol=10
	normal_vol=90

	current_vol=$(osascript -e 'output volume of (get volume settings)')

	if [ "$min_vol" -gt "$current_vol" ]; then
		echo "Volume is under $min_vol, raising it to $normal_vol"
		osascript -e "set volume output volume $normal_vol"
		else
		echo "Volume is over $min_vol, no intervention needed"
	fi
		
}

kill_soft
sleep 1
start_pd
sleep 1
start_reaper
sleep 1
start_chataigne
sleep 1
handle_volume
sleep 4
start_vvvideo

