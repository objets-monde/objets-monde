#/bin/bash
#sleep 1

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

kill_soft()
{
	echo "kill vvvideo"
	killall vvvideo
	echo "kill pd"
	killall pd
	echo "kill REAPER"
	killall REAPER
}

activate_window()
{
window_name="$1"  # Replace with the name of the window you want to activate

osascript <<EOF
tell application "System Events"
    tell process "Your Application Name"  # Replace with the name of the application the window belongs to
        set frontmost to true
        
        # Loop through the windows and activate the one with matching name
        repeat with win in windows
            if name of win is equal to "$window_name" then
                set frontmost of win to true
                exit repeat
            end if
        end repeat
    end tell
end tell
EOF

}

send_message_pd()
{
	echo "$@" | nc -u -w 0 127.0.0.1 20001
}

start_vvvideo()
{
	cd $SCRIPTPATH/../../../../../vvvideo/vvvideo/bin/
	open vvvideo.app
}

start_pd()
{
	cd $SCRIPTPATH/../../../../../om-pd
	open objets-monde.pd
}

start_reaper()
{
	cd $SCRIPTPATH/../../../../../oxider/reaper/
	open objets-monde.RPP
}

set_volume()
{
# 0-100 (100 max)
osascript -e "set volume output volume $1"
}


kill_soft
sleep 1
start_reaper
sleep 1
set_volume 90
sleep 1
start_pd
sleep 6
start_vvvideo
sleep 1
send_message_pd preset read preset/default.txt
send_message_pd init 1


