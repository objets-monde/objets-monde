# Installation Manual V1
[Objets-monde](/docs/)
## Instructions

1. Read this manual
2. [Unbox](#unboxing) material
3. Assemble [interactive interface](#interactive-interface) 
4. Plug [Computer Assembly](#computer-assembly) connectors 
5. [Wall mounts the computer assembly](#wall-mounting) (optional) 
6. Wire the installation according to [wiring schematic](#wiring-schematic)
7. Power on the installation
8. Test [interactions](https://player.vimeo.com/video/774767067?h=bea312cba3&badge=0&autopause=0&player_id=0&app_id=58479)
   * Turning the knob change speed
   * Pushing the knob change scene  
9.  Adjust volume on the speakers
10. Confirm that computer is online and ready for remote access


## Unboxing

![box dimensions](./om-box.drawio.png)
![bag handle](./om-bag-handle.drawio.png)

![Closed bag](./om-bag.drawio.png)

![Opened bag](./om-bag-content.drawio.png)

![Unboxed](./om-unboxed.drawio.png)

## Accessories box

 ![Accessories box](./om-acc-box.drawio.png)

 ![Accessories box packed](./om-acc-packed.drawio.png)

 [![Accessories box content](./om-acc-content.drawio.png)](./om-acc-content.drawio.png)


## Computer Assembly

![Computer Assembly unplugged for shipping](./om-computer-unplugged.drawio.png)

![Computer Assembly replugged ](./om-computer-replugged.drawio.png)

![Computer assembly components](./objets-monde_computer_assembly.drawio.png)

### Wall mounting

![Wall mountings front side](./om-computer-wallmount-front.drawio.png)

![Computer assembly back side](./om-computer-wallmount-back.drawio.png)


## Interactive interface

![Interactive interface](./om-interactive-interface.drawio.png)

### Head

![Interactive interface head](./om-interactive-interface-head.drawio.png)

### Body

![Interactive interface body](./om-interactive-interface-body.drawio.png)

## Wiring schematic

![Wiring schematic](./om-schema-drawio.png)

## Mac mini M1 ports

![Mac Mini ports](./om-macmini.png)

## Troubleshooting

### Computer is not turning on

* Make sure all power cables are correctly seated
* Computer should have a white LED light and the POE injector have a green LED 
* Computer should turn on 
  * If computer does not turn on press Power button on the computer  

### Screen resolution is not 1080p 

####  If screen resolution is something else than 1080p 60 fps, it could lead to a slowdown.

* Plug the EDID spoofer in the HDMI out of the Mac Mini.
* The EDID spoofer is in the accessories bag inside the accessories box

### Rotary encoder stopped working

Press  `reset`  on the [interactive interface body M5POE I2COSC](#body) 

### Rebooting the installation

* Hold at least 8 seconds the power button of the MacMini 
* Wait 5 seconds 
* Push power button
* Boot time should be around 2 minutes


