# Technical Documentation OM_V2-B

[Objets-monde](/docs/)

## Interactive interface OM_V2-B

![Interactive interface](om_v2_b.drawio.png)


## USB Ethernet POE interface

To ensure OSC communication, plug the USB ETHERNET POE interface

![USB_ETHERNET_POE_interface.JPG](usb_ethernet_poe.drawio.png 'USB ETHERNET POE interface')


## Connections diagrams

```mermaid
graph LR
    MacMini --> USB_Ethernet --> POE_INJECTOR --RJ45--> Interactive_Interface 
    AC --> MacMini
    AC --> POE_INJECTOR
```

## Accessories

![](accessories.drawio.png 'Accessories')


## OM_V2-B to OM_V2-A Package


![](retrofit_kit.drawio.png 'Retrofit Kit Content')

## OM_V2-B to OM_V2-A Assembled 

![](om_v2_b_to_a.jpg 'Retrofit Kit Assemble')
