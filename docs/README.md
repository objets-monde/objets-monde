[Objets monde](/)

# Technical Documentation

## Interactive interface

### V2

V2 has a [M5 I2C rotary encoder](https://shop.m5stack.com/products/encoder-unit) instead of a custom PCB assembly on V1


#### V2-A
V2-A refer to the interface that is mounted on a mic stand.

* [V2-A technical documentation](/docs/om_v2/)


![Interactive interface](om_v2/om_v2_a.drawio.png 'OM V2-A Interactive Interface')



#### [V2-B](/docs/om_v2-b/)

V2-B refer to the Fresnoy Monolith version, it can be mounted behind a wall. 

* [V2-B technical documentation ](/docs/om_v2-b/)


![Interactive interface](om_v2/om_v2_b.drawio.png 'OM V2-B Interactive Interface')


### V1

V1 refer to the the first version of the installation, before 2023.

* [V1 technical documentation](/docs/om_v1/)

![Interactive interface](om_v1/om-interactive-interface.drawio.png 'OM V1 Interactive Interface')



## Computer 

### Mac mini M1 ports

![Mac Mini ports](om_v1/om-macmini.png 'Mac Mini M1')



## Troubleshooting

### Computer is not turning on

* Make sure all power cables are correctly seated
* Computer should have a white LED light and the POE injector have a green LED 
* Computer should turn on 
  * If computer does not turn on press Power button on the computer  

### Screen resolution is not 1080p 

####  If screen resolution is something else than 1080p 60 fps, it could lead to a slowdown.

* Plug the EDID spoofer in the HDMI out of the Mac Mini.
* The EDID spoofer is in the accessories bag inside the accessories box

### Rotary encoder stopped working

Press  `reset`  on the Micro Controller

![reset](reset_btn.jpg 'reset button')


### Rebooting the installation

* Hold at least 8 seconds the power button of the MacMini 
* Wait 5 seconds 
* Push power button
* Boot time should be around 2 minutes


